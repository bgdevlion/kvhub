﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderwritingEngine.Domain
{
    public class UnderwriterService
    {
        public bool IsQualified(int age, bool isAustralian)
        {
            return IsOlderThan18(age) && IsAustralian(isAustralian);
        }

        private bool IsOlderThan18(int age)
        {
            return age > 18;
        }

        private bool IsAustralian(bool isAusie)
        {
            return isAusie;
        }
    }
}
