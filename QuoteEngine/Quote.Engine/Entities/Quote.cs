﻿namespace QuoteEngine.Domain.Entities
{
    public class Quote
    {
        public string Product { get; set; }
        public int Premium { get; set; }        
    }
}
