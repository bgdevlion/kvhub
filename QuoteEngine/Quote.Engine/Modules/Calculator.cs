﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuoteEngine.Domain.Entities;
using QuoteEngine.Domain.ViewModels;

namespace QuoteEngine.Domain.Modules
{
    public class Calculator
    {
        public const int MinimumIncome = 36000;
        public Quote CalculatePremium(Lead lead)
        {
            return new Quote { Premium = (int)(0.10 * lead.Income), Product = "Life Innurance" };
        }

        public bool MeetIncomeTest(double income)
        {
            return income > MinimumIncome;
        }
    }
}
