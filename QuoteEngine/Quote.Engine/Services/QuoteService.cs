﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuoteEngine.Domain.Entities;
using QuoteEngine.Domain.Modules;
using QuoteEngine.Domain.ViewModels;
using UnderwritingEngine.Domain;

namespace QuoteEngine.Domain.Services
{
    public class QuoteService
    {
        private Calculator _calculator = new Calculator();

        public Lead GetQuote(Lead lead)
        {
            if (IsValidLead(lead))
            {
                lead.Quote = _calculator.CalculatePremium(lead);
            }
            else
            {
                lead.Errors = BuildErrors(lead);
            }

            return lead;
        }

        private List<string> BuildErrors(Lead lead)
        {
            var uw = new UnderwriterService();
            var results = new List<string>();

            if (!uw.IsQualified(lead.Age, lead.IsAustralian))
            {
                results.Add("You must be over 18 and australian.");
            }
            if (!_calculator.MeetIncomeTest(lead.Income))
            {
                results.Add(string.Format("Minimum income is ${0}.00", Calculator.MinimumIncome));
            }

            return results;
        }

        private bool IsValidLead(Lead lead)
        {
            var underwriter = new UnderwriterService();
            var result = underwriter.IsQualified(lead.Age, lead.IsAustralian)
                            &&
                        _calculator.MeetIncomeTest(lead.Income);

            return result;
        }
    }
}
