﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Xml.Schema;
using QuoteEngine.Domain.Entities;

namespace QuoteEngine.Domain.ViewModels
{
    public class Lead
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public bool IsAustralian { get; set; }
        public double Income { get; set; }
        public Quote Quote { get; set; }
        public List<string>Errors { get; set; } 

        public Lead(string name, string age, string isAustralian, string income)
        {
            Validation(name, age, isAustralian, income);

            Name = name;
            Age = int.Parse(age);
            Income = double.Parse(income);
            IsAustralian = bool.Parse(isAustralian);
        }

        private void Validation(string name, string age, string isAustralian, string income)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("name is empty", name);
            }
            if (string.IsNullOrEmpty(age))
            {
                throw new ArgumentException("age is empty", age);
            }
            if (string.IsNullOrEmpty(isAustralian))
            {
                throw new ArgumentException("citizen is empty", isAustralian);
            }
            if (string.IsNullOrEmpty(income))
            {
                throw new ArgumentException("income is empty", income);
            }
        }
    }
}
