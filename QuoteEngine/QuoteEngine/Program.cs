﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuoteEngine.Domain.Services;
using QuoteEngine.Domain.ViewModels;

namespace QuoteEngine
{
    class Program
    {
        static void Main(string[] args)
        {            
            Console.WriteLine("Enter your name: ");
            var name = Console.ReadLine();
            Console.WriteLine("Enter your age (eg: 18): ");
            var age = Console.ReadLine();
            Console.WriteLine("Are you australian (eg: true/false): ");
            var isAus = Console.ReadLine();
            Console.WriteLine("Enter your income $: ");
            var income = Console.ReadLine();

            try
            {
                var newLead = new Lead(name, age, isAus, income);
                var lead = new QuoteService().GetQuote(newLead);
                DisplayQuote(lead);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Invalid input: " + ex.Message);
            }
        }

        private static void DisplayQuote(Lead lead)
        {
            if (lead.Errors != null)
            {
                Console.WriteLine(string.Format("We are unable to provide you a quote due to the following reasons;"));
                lead.Errors.ForEach(x => Console.WriteLine(string.Format("***{0}", x)));                
            }
            else
            {
                Console.WriteLine(string.Format("The premium for {0} product is ${1}.00", lead.Quote.Product, lead.Quote.Premium));
            }
        }
    }
}
